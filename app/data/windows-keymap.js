export default [
    // -----------------------------------------------------------Refactoring-----------------------------------------------------------
    {
        title: "Copy",
        content: "F5"
    },
    {
        title: "Move",
        content: "F6"
    },
    {
        title: "Safe Delete",
        content: "Alt + Delete"
    },
    {
        title: "Rename",
        content: "Shift + F6"
    },
    {
        title: "Refactor this",
        content: "Ctrl + Alt + Shift + T"
    },
    {
        title: "Change Signature",
        content: "Ctrl + F6"
    },
    {
        title: "Inline",
        content: "Ctrl + Alt + N"
    },
    {
        title: "Extract Method",
        content: "Ctrl + Alt + M"
    },
    {
        title: "Extract Variable",
        content: "Ctrl + Alt + V"
    },
    {
        title: "Extract Field",
        content: "Ctrl + Alt + F"
    },
    {
        title: "Extract Constant",
        content: "Ctrl + Alt + C"
    },
    {
        title: "Extract Parameter",
        content: "Ctrl + Alt + P"
    },
    // -----------------------------------------------------------Navigation-----------------------------------------------------------

    {
        title: "Go to class",
        content: "Ctrl + N"
    },
    {
        title: "Go to file",
        content: "Ctrl + Shift + N"
    },
    {
        title: "Go to symbol",
        content: "Ctrl + Alt + Shift + N"
    },
    {
        title: "Go to next /previous editor tab",
        content: "Alt + Right/Left"
    },
    {
        title: "Go back to previous tool window",
        content: " F12"
    },
    {
        title: "Go to editor (from tool window)",
        content: "Esc"
    },
    {
        title: "Hide active or last active window",
        content: "Shift + Esc"
    },
    {
        title: "Go to line",
        content: "Ctrl + G"
    },
    {
        title: "Recent files popup",
        content: "Ctrl + E"
    },
    {
        title: "Navigate back/ forward",
        content: "Ctrl + Alt + Left/Right"
    },
    {
        title: "Navigate to last edit location",
        content: "Ctrl + Shift + Backspace"
    },
    {
        title: "Select current file or symbol in any view",
        content: "Alt + F1"
    },
    {
        title: "Go to declaration",
        content: "Ctrl + B , Ctrl + Click"
    },
    {
        title: "Go to implementation(s)",
        content: "Ctrl + Alt + B"
    },
    {
        title: "Open quick definition lookup",
        content: "Ctrl + Shift + I"
    },
    {
        title: "Go to type declaration",
        content: "Ctrl + Shift + B"
    },
    {
        title: "Go to super-method/ super-class",
        content: "Ctrl + U"
    },
    {
        title: "Go to previous /next method",
        content: "Alt + Up/Down"
    },
    {
        title: "Move to code block end/ start",
        content: "Ctrl + ]/["
    },
    {
        title: "File structure popup",
        content: "Ctrl + F12"
    },
    {
        title: "Type hierarchy",
        content: "Ctrl + H"
    },
    {
        title: "Method hierarchy",
        content: "Ctrl + Shift + H"
    },
    {
        title: "Call hierarchy",
        content: "Ctrl + Alt + H"
    },
    {
        title: "Next /previous highlighted error",
        content: "F2 / Shift + F2"
    },
    {
        title: "Edit source/View source",
        content: "F4 / Ctrl + Enter"
    },
    {
        title: "Show navigation bar",
        content: "Alt + Home"
    },
    {
        title: "Toggle bookmark",
        content: "F11"
    },
    {
        title: "Toggle bookmark with mnemonic ",
        content: "Ctrl + F11"
    },
    {
        title: "Go to numbered bookmark",
        content: "Ctrl + #[0-9]"
    },
    {
        title: "Show bookmarks",
        content: "Shift + F11"
    },

    // -----------------------------------------------------------Compile and Run-----------------------------------------------------------



    {
        title: "Make project",
        content: "Ctrl + F9"
    },
    {
        title: "Compile selected file, package or module",
        content: "Ctrl + Shift + F9"
    },
    {
        title: "Select configuration and run/debug",
        content: "Alt + Shift + F10/F9"
    },
    {
        title: "Run/Debug",
        content: "Shift + F10 / F9"
    },
    {
        title: "Run context configuration from editor",
        content: "Ctrl + Shift + F10"
    },


    // -----------------------------------------------------------Usage Search-----------------------------------------------------------

    {
        title: "Find usages /Find usages in file",
        content: "Alt + F7 / Ctrl + F7"
    },
    {
        title: "Highlight usages in file",
        content: "Ctrl + Shift + F7"
    },
    {
        title: "Show usages",
        content: "Ctrl + Alt + F7"
    },



    // -----------------------------------------------------------VCS / Local History-----------------------------------------------------------


    {
        title: "Commit project to VCS",
        content: "Ctrl + K"
    },
    {
        title: "Update project from VCS",
        content: "Ctrl + T"
    },
    {
        title: "Push commits",
        content: "Ctrl + Shift + K"
    },
    {
        title: "‘VCS’ quick popup",
        content: "Alt + BackQuote (`)"
    },

    // -----------------------------------------------------------Live Templates-----------------------------------------------------------


    {
        title: "Surround with Live Template",
        content: "Ctrl + Alt + J"
    },
    {
        title: "Insert Live Template",
        content: "Ctrl + J"
    },

    // -----------------------------------------------------------Search / Replace-----------------------------------------------------------

    {
        title: "Search everywhere",
        content: "Double Shift"
    },
    {
        title: "Find",
        content: "Ctrl + F"
    },
    {
        title: "Find next /previous",
        content: "F3 / Shift + F3"
    },
    {
        title: "Replace",
        content: "Ctrl + R"
    },
    {
        title: "Find in path",
        content: "Ctrl + Shift + F"
    },
    {
        title: "Replace in path",
        content: "Ctrl + Shift + R"
    },
    {
        title: "Select next occurrence",
        content: "Alt + J"
    },
    {
        title: "Select all occurrences",
        content: "Ctrl + Alt + Shift + J"
    },
    {
        title: "Unselect occurrence",
        content: "Alt + Shift + J"
    },


    // -----------------------------------------------------------Debugging-----------------------------------------------------------

    {
        title: "Step over / into",
        content: "F8 / F7"
    },
    {
        title: "Smart step into/Step out",
        content: "Shift + F7 / Shift + F8"
    },
    {
        title: "Run to cursor",
        content: "Alt + F9"
    },
    {
        title: "Evaluate expression",
        content: "Alt + F8"
    },
    {
        title: "Resume program",
        content: "F9"
    },
    {
        title: "Toggle breakpoint",
        content: "Ctrl + F8"
    },
    {
        title: "View breakpoints",
        content: "Ctrl + Shift + F8"
    },

    // -----------------------------------------------------------General-----------------------------------------------------------

    {
        title: "Open corresponding tool window",
        content: "Alt + #[0-9]"
    },
    {
        title: "Save all",
        content: "Ctrl + S"
    },
    {
        title: "Synchronize",
        content: "Ctrl + Alt + Y"
    },
    {
        title: "Toggle maximizing editor",
        content: "Ctrl + Shift + F12"
    },
    {
        title: "Inspect current file with current profile",
        content: "Alt + Shift + I"
    },
    {
        title: "Quick switch current scheme",
        content: "Ctrl + BackQuote (`)"
    },
    {
        title: "Open Settings dialog",
        content: "Ctrl + Alt + S"
    },
    {
        title: "Open Project Structure dialog",
        content: ""
    },
    {
        title: "Find Action",
        content: "Ctrl + Shift + A"
    },

    // -----------------------------------------------------------Remember these Shortcuts-----------------------------------------------------------


    {
        title: "Smart code completion",
        content: "Ctrl + Shift + Space"
    },
    {
        title: "Search everywhere",
        content: "Double Shift"
    },
    {
        title: "Show intention actions and quick-fixes",
        content: "Alt + Enter"
    },
    {
        title: "Generate code",
        content: "Alt + Ins"
    },
    {
        title: "Parameter info",
        content: "Ctrl + P"
    },
    {
        title: "Extend selection",
        content: "Ctrl + W"
    },
    {
        title: "Shrink selection",
        content: "Ctrl + Shift + W"
    },
    {
        title: "Recent files popup",
        content: "Ctrl + E"
    },
    {
        title: "Rename",
        content: "Shift + F6"
    },


    // -----------------------------------------------------------Editing-----------------------------------------------------------

    {
        title: "Basic code completion",
        content: "Ctrl + Space"
    },
    {
        title: "Smart code completion",
        content: "Ctrl + Shift + Space"
    },
    {
        title: "Complete statement",
        content: "Ctrl + Shift + Enter"
    },
    {
        title: "Parameter info (within method call arguments)",
        content: "Ctrl + P"
    },
    {
        title: "Quick documentation lookup",
        content: "Ctrl + Q"
    },
    {
        title: "External Doc",
        content: "Shift + F1"
    },
    {
        title: "Brief Info",
        content: "Ctrl + mouse"
    },
    {
        title: "Show descriptions of error at caret",
        content: "Ctrl + F1"
    },
    {
        title: "Generate code...",
        content: "Alt + Insert"
    },
    {
        title: "Override methods",
        content: "Ctrl + O"
    },
    {
        title: "Implement methods",
        content: "Ctrl + I"
    },
    {
        title: "Surround with…",
        content: "Ctrl + Alt + T"
    },
    {
        title: "Comment /uncomment with line comment",
        content: "Ctrl + /"
    },
    {
        title: "Comment /uncomment with block comment",
        content: "Ctrl + Shift + /"
    },
    {
        title: "Extend selection",
        content: "Ctrl + W"
    },
    {
        title: "Shrink selection",
        content: "Ctrl + Shift + W"
    },
    {
        title: "Context info",
        content: "Alt + Q"
    },
    {
        title: "Show intention actions and quick-fixes",
        content: "Alt + Enter"
    },
    {
        title: "Reformat code",
        content: "Ctrl + Alt + L"
    },
    {
        title: "Optimize imports",
        content: "Ctrl + Alt + O"
    },
    {
        title: "Auto-indent line(s)",
        content: "Ctrl + Alt + I"
    },
    {
        title: "Indent /unindent selected lines",
        content: "Tab / Shift + Tab"
    },
    {
        title: "Cut current line to clipboard",
        content: "Ctrl + X , Shift + Delete"
    },
    {
        title: "Copy current line to clipboard",
        content: "Ctrl + C , Ctrl + Insert"
    },
    {
        title: "Paste from clipboard",
        content: "Ctrl + V , Shift + Insert"
    },
    {
        title: "Paste from recent buffers...",
        content: "Ctrl + Shift + V"
    },
    {
        title: "Duplicate current line",
        content: "Ctrl + D"
    },
    {
        title: "Delete line at caret ",
        content: "Ctrl + Y"
    },
    {
        title: "Smart line join",
        content: "Ctrl + Shift + J"
    },
    {
        title: "Smart line split",
        content: "Ctrl + Enter"
    },
    {
        title: "Start new line",
        content: "Shift + Enter"
    },
    {
        title: "Toggle case for word at caret or selected block",
        content: "Ctrl + Shift + U"
    },
    {
        title: "Select till code block end / start",
        content: "Ctrl + Shift + ] / ["
    },
    {
        title: "Delete to word end",
        content: "Ctrl + Delete"
    },
    {
        title: "Delete to word start",
        content: "Ctrl + Backspace"
    },
    {
        title: "Expand/collapse code block",
        content: "Ctrl + NumPad+ / -"
    },
    {
        title: "Expand all",
        content: "Ctrl + Shift + NumPad+"
    },
    {
        title: "Collapse all",
        content: "Ctrl + Shift + NumPad-"
    },
    {
        title: "Close active editor tab",
        content: "Ctrl + F4"
    }
];