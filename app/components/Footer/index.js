import React, { Component } from "react";
import { FooterTab, Button, Icon, Text } from "native-base";

export default class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }

  windowsSelected() {
    alert("Windows selected");
  }

  nixSelected() {
    alert("*nix selected");
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <FooterTab>
        <Button onPress={this.windowsSelected} vertical active>
          <Icon active name="navigate" />
          <Text>Windows</Text>
        </Button>
        <Button onPress={this.nixSelected} vertical>
          <Icon name="person" />
          <Text>Mac/Linux</Text>
        </Button>
      </FooterTab>
    );
  }
}
