import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import CustomFooter from "../Footer";
import CustomContent from "../Content";

export default class RootComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }
  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>IntelliJ Shortcuts</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <CustomContent />
        </Content>
        <Footer>
          <CustomFooter />
        </Footer>
      </Container>
    );
  }
}
