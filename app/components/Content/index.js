import React, { Component } from "react";
import { Content, Accordion } from "native-base";
import dataArray from "../../data/windows-keymap";

export default class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Content padder>
        <Accordion dataArray={dataArray} expanded={0} />
      </Content>
    );
  }
}
